# NODE JS Native Cluster  #

## Required to use: ##
 - [Node.js](https://nodejs.org/en/download/) v10+
 - [Docker](https://docs.docker.com/install/) Optional
 - [docker-compose](https://docs.docker.com/compose/install/) Optional
 
This is a study project to learn how to create a native cluster server with node js

