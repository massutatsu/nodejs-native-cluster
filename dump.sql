create database if not exists clusterTest;

use clusterTest;

create table if not exists subscriber (
  id int unsigned auto_increment primary key,
  data int unsigned not null,
  created_at timestamp not null default current_timestamp
);
