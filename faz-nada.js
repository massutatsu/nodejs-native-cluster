const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;
if (cluster.isMaster) {

  console.log(`Cluster master ${process.pid} is running`);

  // create Clusters
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  // event listner for cluster close
  cluster.on('exit', (worker, code, signal) => {
    console.log(`cluster ${worker.process.pid} died`);
  });
} else {
  // Clusters can share any TCP connection
  // In this case it is an HTTP server
  http.createServer((req, res) => {
    res.end('END\n');
  }).listen(8000);

  console.log(`cluster ${process.pid} started`);
}
