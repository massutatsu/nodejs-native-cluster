const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;
var amqp = require('amqplib/callback_api');

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

if (cluster.isMaster) {

  console.log(`Cluster master ${process.pid} is running`);

  // create Clusters
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  // event listner for cluster close
  cluster.on('exit', (worker, code, signal) => {
    console.log(`cluster ${worker.process.pid} died`);
  });
} else {
  // Clusters can share any TCP connection
  // In this case it is an HTTP server
  http.createServer((req, res) => {
    handle_database(req,res,"cleber");
  }).listen(8000);

  console.log(`cluster ${process.pid} started`);
}
var channel = null
var q = "hello"
amqp.connect('amqp://127.0.0.1', function(err, conn) {
  conn.createChannel(function(err, ch) {
    ch.assertQueue(q, {durable: false});
    // Note: on Node 6 Buffer.from(msg) should be used
    channel = ch;

  });
});

function handle_database(req,res,rand) {
  channel.sendToQueue(q, new Buffer(rand))
  console.log(" [x] Sent '"+rand)
  res.writeHead(200)
  res.end('END\n')
  return
}
