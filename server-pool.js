const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;
const mysql = require('mysql');

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

if (cluster.isMaster) {

  console.log(`Cluster master ${process.pid} is running`);

  // create Clusters
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  // event listner for cluster close
  cluster.on('exit', (worker, code, signal) => {
    console.log(`cluster ${worker.process.pid} died`);
  });
} else {
  // Clusters can share any TCP connection
  // In this case it is an HTTP server
  http.createServer((req, res) => {
    handle_database(req,res);
  }).listen(8000);

  console.log(`cluster ${process.pid} started`);
}
var pool = mysql.createPool({
    connectionLimit : 100, //important
    host: "127.0.0.1",
    user: "root",
    password: "admin",
    database: "clusterTest"
});

function handle_database(req,res) {
    pool.getConnection(function(err,connection){
        if (err) {
          res.writeHead(500);
          res.json({"code" : 100, "status" : "Error in connection database"});
          return;
        }

        var randVal = getRandomArbitrary(1,1000000);
        var sqlInsert = 'INSERT INTO subscriber (data) values ('+randVal+');';
        connection.query(sqlInsert,function(err,rows){
          connection.release();
          if(err) {
            res.writeHead(500);
            res.end('END\n');
          }
          res.writeHead(200);
          res.end('END\n');
        });

        connection.on('error', function(err) {
          res.writeHead(500);
          res.json({"code" : 100, "status" : "Error in connection database"});
          return;
        });
  });
}
